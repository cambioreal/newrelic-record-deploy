#!/usr/bin/env bash
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
NEW_RELIC_API_KEY=${NEW_RELIC_API_KEY:?'NEW_RELIC_API_KEY variable missing.'}
NEW_RELIC_APP_ID=${NEW_RELIC_APP_ID:?'NEW_RELIC_APP_ID variable missing.'}
# Git revision
REVISION=${REVISION:?'REVISION variable missing.'}

run newrelic apm deployment create --applicationId $NEW_RELIC_APP_ID --revision $REVISION
